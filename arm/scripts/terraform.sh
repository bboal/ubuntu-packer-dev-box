#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

apt-get install unzip
mkdir -p /tmp/terraform
cd /tmp/terraform

# Install terraform 1.0's current version
wget -q https://releases.hashicorp.com/terraform/1.0.6/terraform_1.0.6_linux_arm64.zip
unzip -qq -o terraform_1.0.6_linux_arm64.zip
mv terraform /usr/local/bin/terraform
