#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# Install create a temp dir for download files
mkdir /tmp/chrome
cd /tmp/chrome

# already done in setup.sh
# apt-get update
apt --fix-broken -qq -y install

# Download Chrome
apt-get install -y chromium-browser

# Cleanup temp directory
cd /tmp
rm -rf /tmp/chrome
