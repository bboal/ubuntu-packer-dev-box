# Install virtualbox
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -qq -y && sudo apt-get dist-upgrade -qq -y && sudo apt-get autoremove -qq -y

# Install headers
sudo apt-get install -qq -y dkms build-essential linux-headers-`uname -r` gcc make

# Add apt repositories
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"

sudo apt remove --purge -qq -y virtualbox
sudo apt-get install -qq -y virtualbox unzip git-core

sudo /sbin/vboxconfig

# Clone repository
git clone --branch $3 https://gitlab.com/rice.patrick/ubuntu-packer-dev-box.git
cd ./ubuntu-packer-dev-box/amd

# Install packer
wget -q https://releases.hashicorp.com/packer/1.7.4/packer_1.7.4_linux_amd64.zip && unzip ./packer_1.7.4_linux_amd64.zip

# Run packer
export VAGRANT_CLOUD_TOKEN=$2
./packer build ./ubuntu.json

wget -q https://releases.hashicorp.com/vagrant/2.2.18/vagrant_2.2.18_linux_amd64.zip && unzip ./vagrant_2.2.18_linux_amd64.zip
echo "Logging into Vagrant Cloud"
./vagrant cloud auth login
echo "Login success"
echo "Publishing box to Vagrant Cloud..."
# We're going to add a "|| true" to the end of this command since vagrant cloud's service usually doesn't clean itself up properly for larger boxes. 
# We'll release manually if it fails.
export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
./vagrant cloud publish rice-patrick/ubuntu-bionic-dev-box $1 virtualbox testbox_virtualbox.box --force --release || true
