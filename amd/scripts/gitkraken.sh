#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# Install gitkraken
mkdir -p /tmp/gitkraken
cd /tmp/gitkraken

wget -q https://release.gitkraken.com/linux/gitkraken-amd64.deb

# Run fixBroken on apt, because that's required here for some reason
apt-get -y -qq --fix-broken install
apt-get -y -qq install gconf2

# run the package install
apt-get install ./gitkraken-amd64.deb

# Cleanup gitkraken install
cd /tmp
rm -rf /tmp/gitkraken
