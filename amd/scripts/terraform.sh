#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

apt-get install unzip
mkdir -p /tmp/terraform
cd /tmp/terraform

# Install terraform 11 as the backup tf.
wget -q https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip -qq -o terraform_0.11.13_linux_amd64.zip
mv terraform /usr/local/bin/terraform11

# Add sumologic plugin
# This has been removed, since terraform now supports installing sumologic's provider via terraform init

# Install terraform 12 as the main tf.
wget -q https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip
unzip -qq -o terraform_0.12.28_linux_amd64.zip
mv terraform /usr/local/bin/terraform
