#!/bin/sh
export DEBIAN_FRONTEND=noninteractive

# Credits to:
#  - http://vstone.eu/reducing-vagrant-box-size/
#  - https://github.com/mitchellh/vagrant/issues/343

cd /

apt-get autoremove -y

# Remove APT cache
apt-get clean -y
apt-get autoclean -y

# Zero out free space
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

# Remove bash history
unset HISTFILE
rm -f /root/.bash_history
rm -f /home/vagrant/.bash_history

# Cleanup log files
find /var/log -type f | while read f; do echo -ne '' > $f; done;
